package note.model;

public class Elev {
	private int nrMatricol;
	private String nume;
	
	public Elev(int nrMatricol, String nume) {
//		this.setNrmatricol(nrmatricol);
//		this.setNume(nume);
		this.nrMatricol=nrMatricol;
		this.nume=nume;
	}

	public int getNrMatricol() {
		return nrMatricol;
	}

	public void setNrmatricol(int nrmatricol) {
		this.nrMatricol = nrmatricol;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}
	
	
	public String toString() {
		return this.nrMatricol + " " + this.nume;
	}
	
}
