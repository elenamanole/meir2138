package note.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import note.utils.ClasaException;
import note.utils.Constants;

import note.model.Corigent;
import note.model.Elev;
import note.model.Medie;
import note.model.Nota;

public class ClasaRepositoryImplementation implements ClasaRepository{

	private HashMap<Elev, HashMap<String, List<Double>>> clasa;
	
	public ClasaRepositoryImplementation() {
		clasa = new HashMap<Elev, HashMap<String, List<Double>>>();
	}

	@Override
	public void creazaClasa(List<Elev> elevi, List<Nota> note) {
		List<String> materii = new LinkedList<String>();
		for(Nota nota : note) {
			if(!materii.contains(nota.getMaterie()))
					materii.add(nota.getMaterie());
		}
		for (Elev elev : elevi) {
			HashMap<String, List<Double>> situatie = new HashMap<String, List<Double>>();
			for(String materie : materii) {
				List<Double> noteMaterie = new LinkedList<Double>();
				for(Nota nota : note) 
					if(nota.getMaterie().equals(materie) && nota.getNrmatricol() == elev.getNrMatricol())
						noteMaterie.add(nota.getNota());
				situatie.put(materie, noteMaterie);
			}
			clasa.put(elev, situatie);
		}
	}


	@Override
	public void adaugaNota(Elev e,Nota n) throws ClasaException {

		validareNota(n);
		validareElev(e);
		HashMap<String,List<Double>> note=clasa.get(e);
		if(!note.containsKey(n.getMaterie()))
			note.put(n.getMaterie(),new LinkedList<>());
		note.get(n.getMaterie()).add(n.getNota());
	}

    private void validareElev(Elev e) throws ClasaException {
        if(e.getNrMatricol() < Constants.minNrmatricol || e.getNrMatricol() > Constants.maxNrmatricol)
            throw new ClasaException(Constants.invalidNrmatricol);
        if(e.getNume().length() < 1 || e.getNume().length() > 255)
            throw new ClasaException(Constants.invalidNume);
        if(!e.getNume().matches("[a-zA-Z ]+"))
            throw new ClasaException(Constants.invalidNume);
    }

    private void validareNota(Nota nota) throws ClasaException {

        if(nota.getMaterie().length() < 5 || nota.getMaterie().length() > 20)
            throw new ClasaException(Constants.invalidMateria);
        if(nota.getNrmatricol() < Constants.minNrmatricol || nota.getNrmatricol() > Constants.maxNrmatricol)
            throw new ClasaException(Constants.invalidNrmatricol);
        if(nota.getNota() < Constants.minNota || nota.getNota() > Constants.maxNota)
            throw new ClasaException(Constants.invalidNota);
        if(nota.getNota() != (int)nota.getNota())
            throw new ClasaException(Constants.invalidNota);
        if(nota.getNrmatricol() != (int)nota.getNrmatricol())
            throw new ClasaException(Constants.invalidNrmatricol);
    }

	@Override
	public HashMap<Elev, HashMap<String, List<Double>>> getClasa() {
		return clasa;
	}

	private double calculeazaSumaNote(List<Double> noteElev) {
		int nrNote=noteElev.size();
		int i=0;
		double suma=0;
		while(i < nrNote) {
			double nota = noteElev.get(i);
			suma += nota;
			i++;
		}
		return suma;
	}

	@Override
	public List<Medie> calculeazaMedii() throws ClasaException{
		List<Medie> medii = new LinkedList<Medie>();
		if(clasa.size() > 0) {
			for(Elev elev : clasa.keySet()) {
				Medie medie = new Medie();
				medie.setElev(elev);
				int nrMaterii = 0;
				double sumaMedii = 0.0;
				double medieElev = 0.0;
				for(String materie : clasa.get(elev).keySet()) {
					List<Double> noteElev = clasa.get(elev).get(materie);
					int nrNote = noteElev.size();
					if(nrNote >0) {
						nrMaterii++;
						double suma=calculeazaSumaNote(noteElev);
						sumaMedii=sumaMedii+ suma/nrNote;
					}
				}
				medieElev = sumaMedii / nrMaterii;
				medie.setMedie(medieElev);
				medii.add(medie);
			}
		}
		else 
			throw new ClasaException(Constants.emptyRepository);
		return medii;
	}


	public void afiseazaClasa() {
		for(Elev elev : clasa.keySet()) {
			System.out.println(elev);
			for(String materie : clasa.get(elev).keySet()) {
				System.out.println(materie);
				for(double nota : clasa.get(elev).get(materie))
					System.out.print(nota + " ");
			}
		}
	}

	@Override
	public List<Corigent> getCorigenti() {
		List<Corigent> corigenti = new ArrayList<Corigent>();
		if(clasa.size() >0) {
			for(Elev elev : clasa.keySet()) {
				Corigent corigent = new Corigent(elev.getNume(), 0);
				for(String materie : clasa.get(elev).keySet()) {
					List<Double> noteElev = clasa.get(elev).get(materie);
					int nrNote = noteElev.size();
					int i = 0;
					double suma = 0;
					if(nrNote > 0) {
						while(i < nrNote) {
							double nota = noteElev.get(i);
							suma += nota;
							i++;
						}
						double media = suma/i;
						if (media <= 4.5)
							corigent.setNrMaterii(corigent.getNrMaterii() + 1);
					}
				}
				if(corigent.getNrMaterii() > 0) {
					int i = 0;
					while(i < corigenti.size() && corigenti.get(i).getNrMaterii() > corigent.getNrMaterii())
						i++;
					if(i < corigenti.size() && corigenti.get(i).getNrMaterii() == corigent.getNrMaterii()) {
						while(i < corigenti.size() && corigenti.get(i).getNrMaterii() == corigent.getNrMaterii() && corigenti.get(i).getNumeElev().compareTo(corigent.getNumeElev()) < 0)
							i++;
						corigenti.add(i, corigent);
					}
					else
						corigenti.add(i, corigent);
				}
			}
		}
		return corigenti;
	}
	
}
