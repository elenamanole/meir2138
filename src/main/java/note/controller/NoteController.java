package note.controller;

import java.util.HashMap;
import java.util.List;

import note.model.Corigent;
import note.model.Elev;
import note.model.Medie;
import note.model.Nota;
import note.repository.ClasaRepository;
import note.repository.ClasaRepositoryImplementation;
import note.repository.EleviRepository;
import note.repository.EleviRepositoryImplementation;
import note.repository.NoteRepository;
import note.repository.NoteRepositoryImplementation;
import note.utils.ClasaException;
import note.utils.Constants;

public class NoteController {
	private NoteRepository noteRepo;
	private ClasaRepository clasaRepo;
	private EleviRepository eleviRepo;

	public NoteController() {
		noteRepo = new NoteRepositoryImplementation();
		clasaRepo = new ClasaRepositoryImplementation();
		eleviRepo = new EleviRepositoryImplementation();
	}

	public NoteController(NoteRepository noteRepo, ClasaRepository clasaRepo, EleviRepository eleviRepo) {
		this.noteRepo = noteRepo;
		this.clasaRepo = clasaRepo;
		this.eleviRepo = eleviRepo;
	}

	public void addNota(Nota nota) throws ClasaException {
		Elev e=eleviRepo.getElev(nota.getNrmatricol());
		if(e==null)
			throw new ClasaException(Constants.invalidNrmatricol);
		noteRepo.addNota(nota);
		clasaRepo.adaugaNota(e,nota);
	}
	public void addNotaToFile(Nota nota, String fisier){
		noteRepo.writeNote(nota,fisier);
	}
	public void addElev(Elev elev) {
		eleviRepo.addElev(elev);
	}
	
	public void creeazaClasa(List<Elev> elevi, List<Nota> note) {
		clasaRepo.creazaClasa(elevi, note);
	}
	
	public List<Medie> calculeazaMedii() throws ClasaException {
			return clasaRepo.calculeazaMedii();
		
	}
	
	public List<Nota> getNote() {
		return noteRepo.getNote();
	}
	
	public List<Elev> getElevi() {
		return eleviRepo.getElevi();
	}
	
	public HashMap<Elev, HashMap<String, List<Double>>> getClasa() {
		return clasaRepo.getClasa();
	}
	
	public void afiseazaClasa() {
		clasaRepo.afiseazaClasa();
	}
	
	public void readElevi(String fisier) {
		eleviRepo.readElevi(fisier);
	}
	
	public void readNote(String fisier) {
		noteRepo.readNote(fisier);
	}
	
	public List<Corigent> getCorigenti() {
		return clasaRepo.getCorigenti();
	}
}
