package note.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import note.model.Nota;
import note.utils.ClasaException;

import note.model.Corigent;
import note.model.Medie;

import note.controller.NoteController;

//functionalitati
//F01.	 adaugarea unei note la o anumita materie (nr. matricol, materie, nota acordata);
//F02.	 calcularea mediilor semestriale pentru fiecare elev (nume, nr. matricol),
//F03.	 afisarea elevilor coringenti, ordonati descrescator dupa numarul de materii la care nu au promovat şi alfabetic dupa nume.


public class StartApp {

	/**
	 * @param args
	 * @throws ClasaException 
	 */
	public static void main(String[] args) {
		NoteController ctrl = new NoteController();
		List<Medie> medii = new LinkedList<Medie>();
		List<Corigent> corigenti = new ArrayList<Corigent>();
		ctrl.readElevi(args[0]);
		ctrl.readNote(args[1]);
		ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
		boolean terminat = false;
		while(!terminat) {
			System.out.println("1. Adaugare Nota");
			System.out.println("2. Calculeaza medii");
			System.out.println("3. Elevi corigenti");
			System.out.println("4. Iesire");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in) );
		    try {
				int option = Integer.parseInt(br.readLine());
				switch(option) {
                    case 1:
                        //am completat cu add nota
						System.out.println("Numar matricol:");
						int numarMatricol=Integer.parseInt(br.readLine());
						System.out.println("Materie:");
						String materie=br.readLine();
						System.out.println("Nota:");
						double nota=Double.parseDouble(br.readLine());
						Nota notaElev=new Nota(numarMatricol,materie,nota);
						ctrl.addNota(notaElev);
						ctrl.addNotaToFile(notaElev,args[1]);
                        break;
					case 2:
						medii = ctrl.calculeazaMedii();
						for(Medie medie:medii)
							System.out.println(medie);
						break;
					case 3:
						corigenti = ctrl.getCorigenti();
						for(Corigent corigent:corigenti)
							System.out.println(corigent);
						break;
					case 4:
						terminat = true;
						break;
					default:
						System.out.println("Introduceti o optiune valida!");
				        break;
				}
				
			} catch (NumberFormatException e) {
                System.out.println("Optiunea selectata nu este valida.");
			} catch (IOException e) {
                System.out.println("eroare de prelucrare a datelor");
			} catch (ClasaException e){
                System.out.println(e.getMessage());
            }
		}
	}

}
