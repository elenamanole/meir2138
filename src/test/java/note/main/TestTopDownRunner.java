package note.main;

import note.repository.wbt.ClasaRepositoryImplementationTest;
import note.repository.non_valid.ClasaRepositoryImplementationTestNonValid;
import note.repository.valid.ClasaRepositoryImplementationTestValid;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ClasaRepositoryImplementationTest.class
        , ClasaRepositoryImplementationTestNonValid.class
        , ClasaRepositoryImplementationTestValid.class
        , TestTopDown.class})
public class TestTopDownRunner {
}
