package note.main;

import note.model.Elev;
import note.repository.EleviRepositoryImplementation;

import java.util.LinkedList;
import java.util.List;

public class EleviRepositoryImplementationStub extends EleviRepositoryImplementation {

    private List<Elev> elevi;
    public EleviRepositoryImplementationStub() {
       elevi=new LinkedList<Elev>();
    }


    public void addElev(Elev e) {
        elevi.add(e);
    }


    public List<Elev> getElevi() {
       return elevi;
    }


    public Elev getElev(int nrMatricol) {
        for (Elev e: elevi)
            if(e.getNrMatricol()==nrMatricol)
                return e;
        return null;
    }


}
