package note.main;

import edu.emory.mathcs.backport.java.util.Arrays;
import note.controller.NoteController;
import note.model.Elev;
import note.model.Medie;
import note.model.Nota;
import note.repository.ClasaRepositoryImplementation;
import note.repository.EleviRepositoryImplementation;
import note.repository.NoteRepositoryImplementation;
import note.utils.ClasaException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class TestTopDown {

    private ClasaRepositoryImplementation clasaRepo;
    private NoteRepositoryImplementation noteRepo;
    private EleviRepositoryImplementation eleviRepo;

    @Before
    public void setUp() throws Exception {
        clasaRepo=new ClasaRepositoryImplementation();
        noteRepo=new NoteRepositoryImplementation();
        eleviRepo=new EleviRepositoryImplementation();
    }

    /**
     * Integreaza ClasaRepository
     * Testeaza NoteController cu ClasaRepository
     */
    @Test
    public void integrateControllerClasaRepo(){
        EleviRepositoryImplementationStub eleviRepoStub=new EleviRepositoryImplementationStub();
        NoteRepositoryImplementationStub noteRepoStub=new NoteRepositoryImplementationStub();

        NoteController noteController=new NoteController(noteRepoStub,clasaRepo,eleviRepoStub);
        assertEquals(0,noteController.getCorigenti().size());

        Elev e1=new Elev(1,"Elena");
        Elev e2=new Elev(2,"Ana");
        List<Elev> elevi=new ArrayList<>();
        elevi.add(e1);
        elevi.add(e2);

        Nota n1=new Nota(1,"informatica",10);
        Nota n2=new Nota(2,"romana",2);
        List<Nota> note=new ArrayList<>();
        note.add(n1);
        note.add(n2);

        //integreaza ClasaRepository
        noteController.creeazaClasa(elevi,note);
        assertEquals(2,noteController.getClasa().size());
        assertTrue(noteController.getClasa().keySet().contains(e1));
        assertTrue(noteController.getClasa().keySet().contains(e2));
        assertTrue(noteController.getClasa().get(e1).get("informatica").contains(10.0));
        assertTrue(noteController.getClasa().get(e2).get("romana").contains(2.0));

        assertEquals(1,noteController.getCorigenti().size());
        assertEquals("Ana",noteController.getCorigenti().get(0).getNumeElev());
        assertEquals(1,noteController.getCorigenti().get(0).getNrMaterii());
    }

    /**
     * Integreaza   ClasaRepository si
     *              NoteRepository
     * Testeaza NoteController cu ClasaRepository si NoteRepository
     */
    @Test
    public void integrateControllerClasaRepoNoteRepo(){
        EleviRepositoryImplementationStub eleviRepoStub=new EleviRepositoryImplementationStub();

        NoteController noteController=new NoteController(noteRepo,clasaRepo,eleviRepoStub);
        assertEquals(0,noteController.getCorigenti().size());
        assertEquals(0,noteController.getNote().size());

        Elev e1=new Elev(1,"Elena");
        Elev e2=new Elev(2,"Ana");


        Nota n1=new Nota(1,"informatica",10);
        Nota n2=new Nota(2,"romana",2);

        //Integreaza ClasaRepository si NoteRepository

        //folosim stub pentru a adauga elevi
        noteController.addElev(e1);
        noteController.addElev(e2);

        //testam metodele ce apeleaza modulele integrate
        assertEquals(0,noteController.getClasa().size());
        assertEquals(0,noteController.getCorigenti().size());
        assertEquals(0,noteController.getNote().size());
        noteController.creeazaClasa(noteController.getElevi(),noteController.getNote());

        assertEquals(2,noteController.getClasa().size());
        try {
            noteController.addNota(n1);
            assertEquals(1,noteController.getNote().size());
            assertEquals(10.0, noteController.getNote().get(0).getNota(),0);
            assertEquals(1, noteController.getNote().get(0).getNrmatricol());

            noteController.addNota(n2);
            assertEquals(2,noteController.getNote().size());
            assertEquals(2.0, noteController.getNote().get(1).getNota(),0);
            assertEquals(2, noteController.getNote().get(1).getNrmatricol());

            List<Medie> medii2=noteController.calculeazaMedii();
            assertEquals(2,medii2.size());
            assertEquals(1,noteController.getCorigenti().size());
            assertTrue(noteController.getCorigenti().get(0).getNumeElev().equals("Ana"));

        } catch (ClasaException e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Integreaza   ClasaRepository,
     *              NoteRepository,
     *              EleviRepository
     * Testeaza NoteController cu ClasaRepository, NoteRepository si EleviRepository
     */
    @Test
    public void integrateControllerClasaRepoNoteRepoEleviRepo(){

        //pentru toate modulele folosim clase reale
        NoteController noteController=new NoteController(noteRepo,clasaRepo,eleviRepo);
        Elev e1=new Elev(1,"Elena");
        Elev e2=new Elev(2,"Ana");


        Nota n1=new Nota(1,"informatica",10);
        Nota n2=new Nota(2,"romana",2);

        assertEquals(0,noteController.getClasa().size());
        assertEquals(0,noteController.getCorigenti().size());
        assertEquals(0,noteController.getNote().size());

        noteController.addElev(e1);
        noteController.addElev(e2);
        assertEquals(2,noteController.getElevi().size());
        assertEquals("Elena",noteController.getElevi().get(0).getNume());
        assertEquals("Ana",noteController.getElevi().get(1).getNume());
        assertEquals(1,noteController.getElevi().get(0).getNrMatricol());
        assertEquals(2,noteController.getElevi().get(1).getNrMatricol());

        noteController.creeazaClasa(noteController.getElevi(),noteController.getNote());
        assertEquals(2,noteController.getClasa().size());

        try {
            noteController.addNota(n1);
            assertEquals(1,noteController.getNote().size());
            assertEquals(10.0, noteController.getNote().get(0).getNota(),0);
            noteController.addNota(n2);
            assertEquals(2,noteController.getNote().size());
            assertEquals(2.0, noteController.getNote().get(1).getNota(),0);
            List<Medie> medii2=noteController.calculeazaMedii();
            assertEquals(2,medii2.size());
            assertEquals(1,noteController.getCorigenti().size());

        } catch (ClasaException e) {
            e.printStackTrace();
            fail();
        }

    }


}