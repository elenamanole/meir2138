package note.main;

import note.controller.NoteController;
import note.model.Corigent;
import note.model.Elev;
import note.model.Medie;
import note.model.Nota;
import note.repository.*;
import note.repository.non_valid.ClasaRepositoryImplementationTestNonValid;
import note.repository.valid.ClasaRepositoryImplementationTestValid;
import note.utils.ClasaException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import java.util.List;

import static org.junit.Assert.*;
//
//@RunWith(Suite.class)
//@Suite.SuiteClasses({ClasaRepositoryImplementationTest.class,ClasaRepositoryImplementationTestNonValid.class,ClasaRepositoryImplementationTestValid.class})
public class IntegrationBigBangTestController {

    //aceste clase contin testele aferente F01, F02, F03
//    ClasaRepositoryImplementationTestNonValid testNonValid;
//    ClasaRepositoryImplementationTestValid testValid;
//    ClasaRepositoryImplementationTest testWBB;

//    @Before
//    public void setUp(){
//        testNonValid=new ClasaRepositoryImplementationTestNonValid();
//        testValid=new ClasaRepositoryImplementationTestValid();
//        testWBB=new ClasaRepositoryImplementationTest();
//    }

//    @Test(expected= ClasaException.class)
//    public void testIndividualModules() throws Exception {
//        testNonValid.setUp();
//        testNonValid.adaugaNotaTC2_EC();
//        testNonValid.adaugaNotaTC3_EC();
//        testNonValid.adaugaNotaTC5_EC();
//        testNonValid.adaugaNotaTC6_BVA();
//        testNonValid.adaugaNotaTC8_BVA();
//        testNonValid.eleviCorigentiTC();
//        testNonValid.tearDown();
//        testValid.setUp();
//        testValid.adaugaNotaTC1_EC();
//        testValid.adaugaNotaTC3_BVA();
//        testValid.adaugaNotaTC4_BVA();
//        testValid.adaugaNotaTC5_BVA();
//        testValid.adaugaNotaTC7_BVA();
//        testValid.adaugaNotaTC9_BVA();
//        testValid.adaugaNotaTC10_BVA();
//        testValid.eleviCorigentiTC();
//        testValid.tearDown();

//        NoteController noteController=new NoteController();

//    }

    @Test
    public void testController(){
        NoteController noteController=new NoteController();
        Elev e1=new Elev(1,"Elena");
        Elev e2=new Elev(2,"Ana");
        noteController.addElev(e1);
        noteController.addElev(e2);
        noteController.creeazaClasa(noteController.getElevi(),noteController.getNote());
        try {
            noteController.addNota(new Nota(1,"informatica",10));
            noteController.addNota(new Nota(2, "romana",2));
            List<Nota> note=noteController.getNote();
            assertEquals(2,note.size());
            assertTrue(note.get(0).getNota()==10&& note.get(0).getMaterie().equals("informatica"));
            assertTrue(note.get(1).getNota()==2&&note.get(1).getMaterie().equals("romana"));
            List<Medie> medii=noteController.calculeazaMedii();
            assertEquals(2,medii.size());
            List<Corigent> corigenti=noteController.getCorigenti();
            assertEquals(1, corigenti.size());
            assertTrue(corigenti.get(0).getNumeElev().equals("Ana"));
        } catch (ClasaException e) {
            e.printStackTrace();
            fail();
        }

    }


}