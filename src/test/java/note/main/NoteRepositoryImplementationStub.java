package note.main;

import note.model.Nota;
import note.repository.NoteRepositoryImplementation;
import note.utils.ClasaException;

import java.util.LinkedList;
import java.util.List;

public class NoteRepositoryImplementationStub extends NoteRepositoryImplementation {

    private List<Nota> note;

    public NoteRepositoryImplementationStub() {
        note = new LinkedList<Nota>();
    }


    public void addNota(Nota nota) throws ClasaException {
        note.add(nota);
    }


    public List<Nota> getNote() {
        return note;
    }


}
