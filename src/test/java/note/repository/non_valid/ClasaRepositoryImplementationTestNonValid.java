package note.repository.non_valid;

import edu.emory.mathcs.backport.java.util.Arrays;
import note.model.Corigent;
import note.model.Elev;
import note.model.Nota;
import note.repository.ClasaRepositoryImplementation;
import note.utils.ClasaException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ClasaRepositoryImplementationTestNonValid {

    private Elev e1;
    private Elev e2;
    private Elev e3;
    private Elev e4;
    private Elev e5;

    private Nota n1;
    private Nota n2;
    private Nota n3;
    private Nota n4;
    private Nota n5;
    private Nota n6;

    private ClasaRepositoryImplementation repo;
    private List<Elev> elevi;

    @Before
    public void setUp() throws Exception {

        repo=new ClasaRepositoryImplementation();
        elevi=new ArrayList<>();

        e1=new Elev(1,"abc123");
        e2=new Elev(3,"");
        e3=new Elev(4, "Alina Ionescu");

        char[] chars = new char[252];
        Arrays.fill(chars, 'E');
        String s = new String(chars);

        e4=new Elev(8,s+"abcd");  //256
        e5=new Elev(9,"Ion Ionescu");

        n1=new Nota(1,"romana",5);
        n2=new Nota(3,"matematica",7);
        n3=new Nota(4,"informatica",11);
        n4=new Nota(5,"romana",7);
        n5=new Nota(9, "romana",-1);
        n6=new Nota(9, "romana",0);


        elevi.add(e1);
        elevi.add(e2);
        elevi.add(e3);
        elevi.add(e4);
        elevi.add(e5);

        repo.creazaClasa(elevi,new ArrayList<>());
    }

    @After
    public void tearDown() throws Exception {

        repo.getClasa().clear();
        elevi.clear();
        e1=null;
        e2=null;
        e3=null;
        e4=null;
        e5=null;

        n1=null;
        n2=null;
        n3=null;
        n4=null;
        repo=null;
        elevi=null;
    }

    @Test(expected= ClasaException.class)
    public void adaugaNotaTC2_EC() throws ClasaException {
        repo.adaugaNota(e1,n1);
//        int no=repo.getClasa().get(e1).get(n1.getMaterie()).size();
//        assertEquals(0,no);
    }

    @Test(expected= ClasaException.class)
    public void adaugaNotaTC3_EC() throws ClasaException {
        repo.adaugaNota(e2,n2);
//        int no=repo.getClasa().get(e2).get(n2.getMaterie()).size();
//        assertEquals(0,no);
    }

    @Test(expected= ClasaException.class)
    public void adaugaNotaTC5_EC() throws ClasaException {
        repo.adaugaNota(e3,n3);
//        int no=repo.getClasa().get(e3).get(n3.getMaterie()).size();
//        assertEquals(0,no);
    }

    @Test(expected= ClasaException.class)
    public void adaugaNotaTC6_EC() throws ClasaException {
        repo.adaugaNota(e3,n5);
//        int no=repo.getClasa().get(e3).get(n3.getMaterie()).size();
//        assertEquals(0,no);
    }

    @Test(expected= ClasaException.class)
    public void adaugaNotaTC6_BVA() throws ClasaException{
        repo.adaugaNota(e4,n4);
//        int no=repo.getClasa().get(e4).get(n4.getMaterie()).size();
//        assertEquals(0,no);
    }

    @Test(expected= ClasaException.class)
    public void adaugaNotaTC8_BVA() throws ClasaException{
        repo.adaugaNota(e5,n6);
//        int no=repo.getClasa().get(e5).get(n6.getMaterie()).size();
//        assertEquals(0,no);
    }


    //F03 NONVALID
    @Test
    public void eleviCorigentiTC(){
        List<Corigent> corigenti=repo.getCorigenti();
        assertEquals(0, corigenti.size());
    }
}