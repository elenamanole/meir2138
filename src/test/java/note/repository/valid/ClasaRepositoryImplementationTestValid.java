package note.repository.valid;

import edu.emory.mathcs.backport.java.util.Arrays;
import note.model.Corigent;
import note.model.Elev;
import note.model.Nota;
import note.repository.ClasaRepositoryImplementation;
import note.utils.ClasaException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ClasaRepositoryImplementationTestValid {

    private Elev e1;
    private Elev e2;
    private Elev e3;
    private Elev e4;
    private Elev e5;
    private Nota n1;
    private Nota n2;
    private Nota n3;
    private Nota n4;
    private Nota n5;
    private Nota n6;
    private Nota n7;

    private ClasaRepositoryImplementation repo;
    private List<Elev> elevi;


    @Before
    public void setUp() throws Exception {
        repo=new ClasaRepositoryImplementation();
        elevi=new ArrayList<>();

        e1=new Elev(1,"Ion Popescu");
        e2=new Elev(5,"E");

        char[] chars = new char[252];
        Arrays.fill(chars, 'E');
        String s = new String(chars);

        e3=new Elev(6,s+"abc"); //255
        e4=new Elev(7,s+"ab");  //254

        e5= new Elev(9,"Ion Ionescu");

        n1=new Nota(1,"informatica",10);
        n2=new Nota(5, "romana",7);
        n3=new Nota(6,"matematica",8);
        n4=new Nota(7,"informatica",5);
        n5=new Nota(9,"romana",1);
        n6=new Nota(9,"romana",9);
        n7=new Nota(9,"romana",2);

        elevi.add(e1);
        elevi.add(e2);
        elevi.add(e3);
        elevi.add(e4);
        elevi.add(e5);

        repo.creazaClasa(elevi,new ArrayList<>());
    }

    @After
    public void tearDown() throws Exception {
        repo.getClasa().clear();
        e1=null;
        e2=null;
        e3=null;
        e4=null;
        e5=null;
        n1=null;
        n2=null;
        n3=null;
        n4=null;
        repo=null;
        elevi=null;
    }

    @Test
    public void adaugaNotaTC1_EC() {
        try {
            repo.adaugaNota(e1,n1);
            int no=repo.getClasa().get(e1).get(n1.getMaterie()).size();
            assertEquals(1,no);
        } catch (ClasaException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void adaugaNotaTC3_BVA() {
        try {
            repo.adaugaNota(e2,n2);
            int no=repo.getClasa().get(e2).get(n2.getMaterie()).size();
            assertEquals(1,no);
        } catch (ClasaException e) {
            e.printStackTrace();
            fail();
        }

    }

    @Test
    public void adaugaNotaTC4_BVA() {
        try {
            repo.adaugaNota(e3,n3);
            int no=repo.getClasa().get(e3).get(n3.getMaterie()).size();
            assertEquals(1,no);
        } catch (ClasaException e) {
            e.printStackTrace();
            fail();
        }

    }

    @Test
    public void adaugaNotaTC5_BVA() {
        try {
            repo.adaugaNota(e4,n4);
            int no=repo.getClasa().get(e4).get(n4.getMaterie()).size();
            assertEquals(1,no);
        } catch (ClasaException e) {
            e.printStackTrace();
            fail();
        }

    }

    @Test
    public void adaugaNotaTC7_BVA() {
        try {
            repo.adaugaNota(e5,n7);
            int no=repo.getClasa().get(e5).get(n5.getMaterie()).size();
            assertEquals(1,no);
        } catch (ClasaException e) {
            e.printStackTrace();
            fail();
        }

    }

    @Test
    public void adaugaNotaTC9_BVA() {
        try {
            repo.adaugaNota(e5,n5);
            int no=repo.getClasa().get(e5).get(n5.getMaterie()).size();
            assertEquals(1,no);
        } catch (ClasaException e) {
            e.printStackTrace();
            fail();
        }

    }

    @Test
    public void adaugaNotaTC10_BVA() {
        try {
            repo.adaugaNota(e5,n6);
            int no=repo.getClasa().get(e5).get(n6.getMaterie()).size();
            assertEquals(1,no);
        } catch (ClasaException e) {
            e.printStackTrace();
            fail();
        }
    }

    //F03 VALID
    @Test
    public void eleviCorigentiTC(){
        try {
            repo.adaugaNota(e1, n1);  //info 10
            repo.adaugaNota(e1,n5);   //romana 1
            repo.adaugaNota(e1,n7);   //romana 2

            repo.adaugaNota(e2,n2);  //romana 7

            repo.adaugaNota(e5,n7); //romana 2

            List<Corigent> corigenti=repo.getCorigenti();

            //e1 si e5 sunt corigenti la cate o materie, ordinea lor alfabetica e e5,e1
            assertEquals(2,corigenti.size());
            assertEquals(e5.getNume(),corigenti.get(0).getNumeElev());
            assertEquals(1,corigenti.get(0).getNrMaterii());

            assertEquals(e1.getNume(),corigenti.get(1).getNumeElev());
            assertEquals(1,corigenti.get(1).getNrMaterii());

        }catch (ClasaException e){
            e.printStackTrace();
            fail();
        }
    }
}