package note.repository.wbt;

import note.model.Elev;
import note.model.Medie;
import note.model.Nota;
import note.repository.ClasaRepositoryImplementation;
import note.utils.ClasaException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.Float.NaN;
import static org.junit.Assert.*;

public class ClasaRepositoryImplementationTest {

    private ClasaRepositoryImplementation repo;

    @Before
    public void setUp() throws Exception {

        repo=new ClasaRepositoryImplementation();
    }

    @After
    public void tearDown() throws Exception {

        repo.getClasa().clear();
        repo=null;
    }

    @Test(expected=ClasaException.class)
    public void calculeazaMedii_TC01() throws ClasaException {

        List<Medie> medii=repo.calculeazaMedii();
    }

    @Test
    public void calculeazaMedii_TC02() {

        Elev e1=new Elev(1,"Ion Ionescu");
        List<Elev> elevi=new ArrayList<>();
        elevi.add(e1);
        repo.creazaClasa(elevi,new ArrayList<>());
        try {
            List<Medie> medii=repo.calculeazaMedii();
            assertEquals(1,medii.size());
            assertEquals(e1,medii.get(0).getElev());
            boolean medie=Double.isNaN(medii.get(0).getMedie());
            assertTrue(medie);
        } catch (ClasaException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void calculeazaMedii_TC03() {

        Elev e1=new Elev(1,"Ion Ionescu");
        Elev e2=new Elev(2,"Maria Marinescu");
        List<Elev> elevi=new ArrayList<>();
        elevi.add(e1);
        elevi.add(e2);

        Nota n=new Nota(1,"romana",7);
        List<Nota> note=new ArrayList<>();
        note.add(n);

        repo.creazaClasa(elevi,note);

        try {
            List<Medie> medii=repo.calculeazaMedii();
            List<Medie> sorted=medii.stream().sorted((m1,m2)->m1.getElev().getNrMatricol()-m2.getElev().getNrMatricol()).collect(Collectors.toList());

            assertEquals(2,sorted.size());
            assertEquals(e1,sorted.get(0).getElev());
            assertEquals(7.00d,sorted.get(0).getMedie(),0);
            assertEquals(e2,sorted.get(1).getElev());
            boolean medie=Double.isNaN(sorted.get(1).getMedie());
            assertTrue(medie);

        } catch (ClasaException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void calculeazaMedii_TC04() {

        Elev e1=new Elev(1,"Ion Ionescu");
        List<Elev> elevi=new ArrayList<>();
        elevi.add(e1);
        Nota n=new Nota(1,"romana",10);
        List<Nota> note=new ArrayList<>();
        note.add(n);
        repo.creazaClasa(elevi,note);

        try {
            List<Medie> medii=repo.calculeazaMedii();
            assertEquals(1,medii.size());
            assertEquals(e1,medii.get(0).getElev());
            assertEquals(10.00d,medii.get(0).getMedie(),0);
        } catch (ClasaException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void calculeazaMedii_TC05() {

        Elev e1=new Elev(1,"Ion Ionescu");
        Elev e2=new Elev(2,"Maria Marinescu");
        List<Elev> elevi=new ArrayList<>();
        elevi.add(e1);
        elevi.add(e2);
        Nota n1=new Nota(1,"romana",10);
        Nota n2=new Nota(2,"matematica",9);
        List<Nota> note=new ArrayList<>();
        note.add(n1);
        note.add(n2);

        repo.creazaClasa(elevi,note);

        try {
            List<Medie> medii=repo.calculeazaMedii();
            assertEquals(2,medii.size());
            List<Medie> sorted=medii.stream().sorted((m1,m2)->m1.getElev().getNrMatricol()-m2.getElev().getNrMatricol()).collect(Collectors.toList());
            assertEquals(e1,sorted.get(0).getElev());
            assertEquals(10.0d,sorted.get(0).getMedie(),0);
            assertEquals(e2,sorted.get(1).getElev());
            assertEquals(9.0d,sorted.get(1).getMedie(),0);
        } catch (ClasaException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void calculeazaMedii_TC06() {
        Elev e1=new Elev(1,"Ion Ionescu");
        Elev e2=new Elev(2,"Maria Marinescu");
        Elev e3=new Elev(3,"Ana Popescu");
        List<Elev> elevi=new ArrayList<>();
        elevi.add(e1);
        elevi.add(e2);
        elevi.add(e3);

        Nota n1=new Nota(1,"romana",10);
        Nota n2=new Nota(1,"romana",8);
        Nota n3=new Nota(1,"istorie",9);
        Nota n4=new Nota(1,"istorie",7);

        Nota n5=new Nota(2,"romana",4);
        Nota n6=new Nota(2,"matematica",9);
        Nota n7=new Nota(2,"informatica",9);
        Nota n8=new Nota(2,"informatica",9);
        Nota n9=new Nota(2,"informatica",8);

        Nota n10=new Nota(3,"fizica",8);

        List<Nota> note=Arrays.asList(n1,n2,n3,n4,n5,n6,n7,n8,n9,n10);

        repo.creazaClasa(elevi,note);

        try {
            List<Medie> medii=repo.calculeazaMedii();
            assertEquals(medii.size(),3);
            List<Medie> sorted=medii.stream().sorted((m1,m2)->m1.getElev().getNrMatricol()-m2.getElev().getNrMatricol()).collect(Collectors.toList());
            assertEquals(e1,sorted.get(0).getElev());
            assertEquals(8.5d,sorted.get(0).getMedie(),0);
            assertEquals(e2,sorted.get(1).getElev());
            assertEquals(7.222222222222221d,sorted.get(1).getMedie(),0.0);
            assertEquals(e3,sorted.get(2).getElev());
            assertEquals(8d,sorted.get(2).getMedie(),0);

        } catch (ClasaException e) {
            e.printStackTrace();
            fail();
        }
    }
}